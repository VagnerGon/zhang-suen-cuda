//COMENTARIO DO CODIGO: C�digo "final"

//Incluindo bibliotecas
#include "ZhangSuen.h"
#include <stdio.h>
#include <time.h>
#include <cuda.h>
//Ainda n�o descobri o que essa biblioteca do cuda faz
#include <cuda_runtime.h>

//Defines para ligar e pausar o relogio
#define LIGA_RELOGIO cudaEventRecord(start, 0);
#define PAUSA_RELOGIO cudaEventRecord(stop, 0);	cudaEventSynchronize(stop);	cudaEventElapsedTime(&time, start, stop);	acum_time += time;

void main(int argc, char **argv)
{
	
//Declara��o de Vari�veis
	clock_t start_t,finish_t;  
	cudaEvent_t start, stop;
	float acum_time=0, time=0 , interval;
	IplImage *image, *clone;
	unsigned int i= 1, *CUDAcmp, N, blockSize, nBlocks, dev=0, haDiferencas,nCmp,k;
	int deviceCount, width, widthStep, nChannels , height;
	unsigned char *img, *CUDAImg_data, *CUDAClone_data, *CUDAClone2_data, *init;
	cudaDeviceProp deviceProp;

//Testes para saber se h� algum dispositivo CUDA instalado e funcionando	
	cudaError_t error_id = cudaGetDeviceCount(&deviceCount);
	
	if (error_id != cudaSuccess)
    	{
		printf("cudaGetDeviceCount returned %d\n-> %s\n", (int)error_id, cudaGetErrorString(error_id));
		exit(EXIT_FAILURE);
    	}

	//COMENTARIO DO CODIGO: This function call returns 0 if there are no CUDA capable devices.
    	if (deviceCount == 0)
    	{
		printf("There are no available device(s) that support CUDA\n");
		exit(EXIT_FAILURE);
    	}
 
//Define o dispositivo CUDA a ser usado   	
	cudaSetDevice(dev);
//Armazena em deviceProp as propriedades do dispositivo CUDA
    	cudaGetDeviceProperties(&deviceProp, dev);
//Reseta a placa de v�deo, destruindo e limpando todos os recursos associados atualmente ao dispositivo
	cudaDeviceReset();

//Carregando a imagem e seus atributos (Utilizando OpenCV)	
	image = cvLoadImage(argv[1]);
	img = (unsigned char *)image->imageData;
	height = image->height;
	width = image->width;
	widthStep = image->widthStep;
	nChannels = image->nChannels;
	nCmp = height * width;
	N = (nCmp * nChannels);

//Calculando o Tamanho e a quantidade de blocos a serem usados (ainda n�o entendi muito bem essa parte)
	//COMENTARIO DO CODIGO: cuda code - alocando memorias
	blockSize = deviceProp.maxThreadsPerBlock;
	nBlocks = N/blockSize + (N%blockSize == 0 ? 0:1);
	blockSize = height-1;
	nBlocks = 1;

//Aloca��o de mem�ria
	cudaMalloc((void**)&CUDAImg_data, sizeof(unsigned char) * N );
	cudaMalloc((void**)&CUDAClone_data, sizeof(unsigned char) * N );
	cudaMalloc((void**)&CUDAClone2_data, sizeof(unsigned char) * N );
	cudaMalloc((void**)&CUDAcmp, sizeof(unsigned int));

//Pelo que eu entendi aloca mem�ria no Host mas que pode ser acessada com maior facilidade pelo Device
	//cudaMallocHost((void**)&j , sizeof(unsigned int) * N);

	init = CUDAImg_data;
	
	//Copiando dados da imagem original do computador para a placa de video
	cudaMemcpy(CUDAImg_data, img, sizeof(unsigned char) * N , cudaMemcpyHostToDevice);

//Criando e Ligando o Rel�gio
	//COMENTARIO DO CODIGO: criando relogio
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	//COMENTARIO DO CODIGO:liga relogio
	LIGA_RELOGIO;
	start_t = clock();	

	//passo1 e passo 2
	passo(CUDAImg_data, CUDAClone_data, 1, blockSize, nBlocks, N, height, width, widthStep, nChannels);
	passo(CUDAClone_data, CUDAClone2_data, 2, blockSize, nBlocks, N, height, width, widthStep, nChannels);

	//Chama a fun��o comparacao_paralelaCPP do arquivo ZhangSuen.cu	
	comparacao_paralelaCPP(nBlocks, blockSize, CUDAClone_data, CUDAClone2_data, CUDAcmp, N, height, width, widthStep, nChannels);
	cudaDeviceSynchronize();	


	//COMENTARIO DO CODIGO: movendo memoria da comparacao do device pro host
	cudaMemcpy(&haDiferencas, CUDAcmp, sizeof(unsigned int), cudaMemcpyDeviceToHost);

	while( haDiferencas)
	{
		//voltando relogio
		//LIGA_RELOGIO;
	
		//passo1 e passo 2
		passo(CUDAClone2_data, CUDAClone_data, 1, blockSize, nBlocks, N, height, width, widthStep, nChannels);
		passo(CUDAClone_data, CUDAClone2_data, 2, blockSize, nBlocks, N, height, width, widthStep, nChannels);

		//cudaDeviceSynchronize();

		cudaMemset(CUDAcmp, 0, sizeof(unsigned int));
		comparacao_paralelaCPP(nBlocks, blockSize, CUDAClone_data, CUDAClone2_data, CUDAcmp, N, height, width, widthStep, nChannels);
		cudaDeviceSynchronize();		
	
		//pausando relogio
		//PAUSA_RELOGIO;
		//movendo memoria da comparacao do device pro host
		cudaMemcpy(&haDiferencas, CUDAcmp, sizeof(unsigned int), cudaMemcpyDeviceToHost);

		i++;
	}

	//COMENTARIO DO CODIGO: pausando relogio
	PAUSA_RELOGIO;
	printf("%d tempo CUDA: %f - %f ms\n", i, acum_time,time);
	
	finish_t = clock() - start_t;
	interval = finish_t / (double)CLOCKS_PER_SEC; 
	printf("tempo TOTAL: %f ms \n", interval * 1000 );

	clone = cvCloneImage(image);
	cudaMemcpy(clone->imageData, CUDAClone2_data, sizeof(unsigned char) * N, cudaMemcpyDeviceToHost);
	//COMENTARIO DO CODIGO: nomeia a janela
	//COMENTARIO DO CODIGO: cvSaveImage("fffggghhh.tiff", clone);
	
	cvNamedWindow("Original");
	cvNamedWindow("Esqueleto");

	//COMENTARIO DO CODIGO: poe a imagem na janela
	cvShowImage("Original", image);
	cvShowImage("Esqueleto", clone);
	
	cvReleaseImage(&image);
	cvReleaseImage(&clone);

	//COMENTARIO DO CODIGO: liberando memoria CUDA
	cudaFree(init);
	cudaFree(CUDAClone_data);
	cudaFree(CUDAClone2_data);
	cudaFree(CUDAcmp);
	cvWaitKey(100000);

}
