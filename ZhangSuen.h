#ifndef __ZhangSuen_h_
#define __ZhangSuen_h_

//Incluindo biblioteca do OpenCV
#include <opencv2\highgui\highgui.hpp>

//COMENTARIO DO CODIGO: realiza o x-�simo passo do algoritmo com suas devidas verifica��es
//COMENTARIO DO CODIGO: e retira os pontos marcados pela funcao "passo"
//Cabe�alho da Fun��o passo que est� no arquivo ZhangSuen.cpp
void passo(unsigned char *imgData, unsigned char *cloneData, int passo, int blockSize, int nBlocks, int tam , int height, int width , int widthStep , int nChannels);

IplImage* passoteste(IplImage* image , int passo );

//Cabe�alho da Fun��o passoCPP que est� no arquivo ZhangSuen.cu
extern "C" void passoCPP(int noOfBlock,int blockSize, unsigned char *img, unsigned char *clone, int N , int height, int width , int widthStep , int nChannels, int passo);

//Cabe�alho da Fun��o comparacao_paralelaCPP que est� no arquivo ZhangSuen.cu
extern "C" void comparacao_paralelaCPP(int noOfBlock, int blockSize, unsigned char *img, unsigned char *clone, unsigned int *cmp, int N , int height, int width , int widthStep , int nChannels);

#endif
