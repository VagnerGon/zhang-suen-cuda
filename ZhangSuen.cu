//incluindo bibliotecas
#include <stdio.h>
#include <cuda.h>


//Pelo que entendi isso � uma diretiva de compila��o onde essa sequ�ncia de defines s� vai ser compilada se o nome da macro n�o tiver sido definido em nenhum outro lugar.
#ifndef _utilidades__

//Defines para localizar a posi��o do pixel e seus 8 vizinhos 
#define _utilidades__
	#define CALCULAINDICE( widthStep , nChannels , linha , coluna )  ((linha)*(widthStep))+((coluna)*(nChannels))
	#define SETARGB( img , r , rval, gval, bval)  img[r] = rval ; img[r+1] = gval ; img[r+2] = bval 
	#define P(x) p[x-1]
	#define CALC_P1(widthStep,nChannels, linha , coluna) P(1) = CALCULAINDICE(widthStep, nChannels, linha, coluna);
	#define CALC_P2(widthStep,nChannels, linha , coluna) P(2) = CALCULAINDICE(widthStep, nChannels, (linha-1), coluna);
	#define CALC_P3(widthStep,nChannels, linha , coluna) P(3) = CALCULAINDICE(widthStep, nChannels, (linha-1), (coluna+1));
	#define CALC_P4(widthStep,nChannels, linha , coluna) P(4) = CALCULAINDICE(widthStep, nChannels, linha, (coluna+1));
	#define CALC_P5(widthStep,nChannels, linha , coluna) P(5) = CALCULAINDICE(widthStep, nChannels,(linha+1), (coluna+1));
	#define CALC_P6(widthStep,nChannels, linha , coluna) P(6) = CALCULAINDICE(widthStep, nChannels,(linha+1), coluna);
	#define CALC_P7(widthStep,nChannels, linha , coluna) P(7) = CALCULAINDICE(widthStep, nChannels,(linha+1), (coluna-1));
	#define CALC_P8(widthStep,nChannels, linha , coluna) P(8) = CALCULAINDICE(widthStep, nChannels, linha, (coluna-1));
	#define CALC_P9(widthStep,nChannels, linha , coluna) P(9) = CALCULAINDICE(widthStep, nChannels, (linha-1),(coluna-1));
	#define CALC_PS(widthStep,nChannels, linha , coluna) CALC_P1(widthStep,nChannels, linha , coluna); CALC_P2(widthStep,nChannels, linha , coluna); CALC_P3(widthStep,nChannels, linha , coluna); CALC_P4(widthStep,nChannels, linha , coluna); CALC_P5(widthStep,nChannels, linha , coluna); CALC_P6(widthStep,nChannels, linha , coluna); CALC_P7(widthStep,nChannels, linha , coluna); CALC_P8(widthStep,nChannels, linha , coluna); CALC_P9(widthStep,nChannels, linha , coluna);
#endif

//Kernel (executando uma linha da imagem por thread)
//COMENTARIO DO CODIGO: passo, eh qual o passo a ser executado, o primeiro ou o segundo (1 ou 2)
__global__ void passo(unsigned char *img, unsigned char *clone, int N , int height, int width , int widthStep , int nChannels, int passo , int blockSize)
{
	int i,j, soma, c, k, p[9], indice, index;
    	
//Calculando o indice da thread	
	index = blockIdx.x * blockDim.x + threadIdx.x;

//Enquanto a linha n�o acabar	
	for(j=0 ; j < width ; j++)
	{		
		indice = CALCULAINDICE( widthStep , nChannels , index+1 , j );

		//COMENTARIO DO CODIGO: pixel central tem que ser preto
		if (img[indice] < 128)
			continue;

//Localiza e armazena todos os pixels necess�rios para o processamento (pixel central e seus vizinhos)
		//COMENTARIO DO CODIGO: populando todos pixels em volta
		CALC_PS(widthStep,nChannels, index+1 , j);

		//COMENTARIO DO CODIGO: a conectividade tem que ser == 1
		for (soma = 0, k = 2 ; k < 9 ; k++) 
			soma += ( (img[P(k)] == 0) * (img[P(k+1)] >= 128) );
		soma += ( (img[P(9)] == 0) * (img[P(2)] >= 128) );
		if (soma != 1)
			continue;

		//COMENTARIO DO CODIGO: vizinhos pretos >=2 && <=6
		for (soma = 0 ,  k = 2 ; k < 10 ; k++)
			soma += (img[P(k)] > 128) ? 1 : 0; 
		if (soma < 2 || soma > 6)
			continue;

		//COMENTARIO DO CODIGO: Verifica se algum vizinho eh zero
		if (passo == 1)
			c = ( (img[P(2)] && img[P(8)]) || 
					!(img[P(4)] * img[P(6)] ) ) ;
		else
			c = ( (img[P(4)] && img[P(6)]) || 
					!(img[P(2)] * img[P(8)] ) ) ;
		if (!c)
			continue;

		//COMENTARIO DO CODIGO: apagando
		SETARGB(clone,indice,0,0,0);
	}
}


// compara a memoria em paralelo, se algo for diferente , retorna 1 no cmp, senao, retorna 0
// cmp tem que comecar com 1 antes de chamar esse metodo
__global__ void comparacao_paralela(unsigned char *img, unsigned char *clone, volatile unsigned int *cmp, int N , int height, int width , int widthStep , int nChannels, int blockSize)
{
	unsigned int indice , index,j;
	volatile __shared__ bool haDiferenca;

    index = blockIdx.x * blockDim.x + threadIdx.x;

	if(threadIdx.x == 0)
		haDiferenca = false;

	__syncthreads();

	if(index < N)
	{
		for(j=0 ; j < width ; j++)
		{
			if(!haDiferenca){

				indice = CALCULAINDICE( widthStep , nChannels , index+1 , j );
			
				bool diferenca = img[indice] - clone[indice];

				// if I found it, tell everyone they can exit
				if(diferenca){
					haDiferenca = true;
					*cmp = 1;
				}

				// if someone in another block found it, tell 
				// everyone in my block they can exit
				if(threadIdx.x == 0 && *cmp == 1)
					haDiferenca = true;

				__syncthreads();
			}
		}
	}
}

//Aqui fica o conte�do das fun��es que tem o cabe�alho l� no arquivo ZhangSuen.h
//S�o essas fun��es que chamam a execu��o do kernel passo e do kernel comparacao_paralela
//COMENTARIO DO CODIGO: Internal memory allocation
extern "C" void passoCPP(int noOfBlock,int blockSize,unsigned char *img, unsigned char *clone , int N , int height, int width, int widthStep , int nChannels, int nPasso)
{
//Chama o kernel passo
	passo <<< noOfBlock, blockSize >>> (img, clone, N , height , width, widthStep , nChannels, nPasso, blockSize);
}

//COMENTARIO DO CODIGO: Internal memory allocation
extern "C" void comparacao_paralelaCPP(int noOfBlock, int blockSize, unsigned char *img, unsigned char *clone, unsigned int *cmp, int N , int height, int width , int widthStep , int nChannels)
{
//Chama o kernel comparacao_paralela
	comparacao_paralela <<< noOfBlock, blockSize >>> (img, clone, cmp, N , height , width, widthStep , nChannels, blockSize);
}

